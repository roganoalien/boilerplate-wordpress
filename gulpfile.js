var gulp            = require('gulp'),
    theme           = require('./theme-config.js'),
    autoprefixer    = require('gulp-autoprefixer'),
    cleanCSS        = require('gulp-clean-css'),
    concat          = require('gulp-concat'),
    del             = require('del'),
    plumber         = require('gulp-plumber'),
    // pug             = require('gulp-pug'),
    rename          = require("gulp-rename"),
    // server          = require('gulp-server-livereload'),
    shell           = require('gulp-shell'),
    stylus          = require('gulp-stylus'),
    uglify          = require('gulp-uglify'),
    watch           = require('gulp-watch');

//Vars from theme-config.js
var $myTheme    = 'wordpress/wp-content/themes/' + theme.name + '/',
    $assets     = $myTheme + 'assets',
    $css        = $myTheme + 'css',
    $fonts      = $myTheme + 'fonts',
    $include    = $myTheme + 'include',
    $scripts    = $myTheme + 'scripts';

//-------------------------||
//  REMOVE GIT REPOSITORY  ||
//-------------------------||
//Erase  Repository
gulp.task('erase', function(){
    return del([
        '.git/**/*',
        '.git/'
    ]);
});

//----------------------------------------------------||
//  DELETE EVERY FILE, CREATE FOLDERS AND COPY FILES  ||
//----------------------------------------------------||
//Delete every Theme before compiling
gulp.task('remove', function(){
    return del([
        'wordpress/wp-content/themes/**/*',
        '!wordpress/wp-content/themes/',
        '!wordpress/wp-content/themes/twentyseventeen',
        '!wordpress/wp-content/themes/twentysixteen',
    ]);
});
//Create all folders for the Theme
gulp.task('create', ['remove'], function(){
    return gulp.src('*.js', { read: false })
    .pipe(shell([
        'mkdir -p ' + $assets+' ' + $css+' ' + $fonts+' ' + $include+' ' + $scripts+' '
    ]));
});
//Copy Assets, Fonts, Include, Style.css and every PHP file on ./theme
gulp.task('copy', ['create'], function(){
    gulp.src(['theme/assets/**/*'])
        .pipe(gulp.dest($assets));
    gulp.src(['theme/fonts/**/*'])
        .pipe(gulp.dest($fonts));
    gulp.src(['theme/include/**/*'])
        .pipe(gulp.dest($include));
    //copy style.css
    gulp.src(['theme/style.css'])
        .pipe(gulp.dest($myTheme));
    //copies every php file on ./theme
    gulp.src(['theme/*.php'])
        .pipe(gulp.dest($myTheme));
});
gulp.task('copy-assets', function(){
    gulp.src(['theme/assets/**/*'])
        .pipe(gulp.dest($assets));
    gulp.src(['theme/fonts/**/*'])
        .pipe(gulp.dest($fonts));
    gulp.src(['theme/include/**/*'])
        .pipe(gulp.dest($include));
    //copy style.css
    gulp.src(['theme/style.css'])
        .pipe(gulp.dest($myTheme));
    //copies every php file on ./theme
    gulp.src(['theme/*.php'])
        .pipe(gulp.dest($myTheme));
});
//Globalize remove, create and copy tasks
gulp.task('start', ['remove', 'create', 'copy']);

//--------------------------------------||
//  STYLUS, AUTOPREFIX AND MINIFY TASK  ||
//--------------------------------------||
gulp.task('stylus', ['start'], function(){
    return gulp.src('theme/stylus/main.styl')
        .pipe(plumber())
        .pipe(stylus())
        .pipe(autoprefixer({
            browsers: ['> 5%'],
            cascade: false
        }))
        .pipe(cleanCSS({compatibility: 'ie9'}))
        .pipe(rename('main.min.css'))
        .pipe(gulp.dest($css));
});
gulp.task('style-watch', function(){
    return gulp.src('app/stylus/style.styl')
        .pipe(plumber())
        .pipe(stylus())
        .pipe(autoprefixer({
            browsers: ['> 5%'],
            cascade: false
        }))
        .pipe(cleanCSS({compatibility: 'ie9'}))
        .pipe(rename('main.min.css'))
        .pipe(gulp.dest($css));
});
gulp.task('styles', ['stylus']);

//----------------------------------||
//  CONCATENATE VENDORS, UGLIFY JS  ||
//----------------------------------||
gulp.task('concatenate', ['start'], function(){
    return gulp.src(['theme/scripts/vendor/jquery-3.2.1.js', 'theme/scripts/vendor/libs/**/*.js'])
        .pipe(plumber())
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(rename('vendor.min.js'))
        .pipe(gulp.dest($scripts + '/vendor'));
});
gulp.task('ugly', ['start', 'concatenate'], function(){
    return gulp.src('theme/scripts/main.js')
        .pipe(plumber())
        .pipe(uglify())
        .pipe(rename('main.min.js'))
        .pipe(gulp.dest($scripts));
});
gulp.task('ugly-js', ['vendor'], function(){
    return gulp.src('theme/scripts/main.js')
        .pipe(plumber())
        .pipe(uglify())
        .pipe(rename('main.min.js'))
        .pipe(gulp.dest($scripts));
});
gulp.task('vendor', function(){
    return gulp.src(['theme/scripts/vendor/*.js', 'theme/scripts/vendor/libs/**/*.js'])
    .pipe(concat('vendor.js'))
    .pipe(uglify())
    .pipe(rename('vendor.min.js'))
    .pipe(gulp.dest($scripts + '/vendor'));
});
gulp.task('scripts', ['concatenate', 'ugly']);

//------------||
//  WATCHERS  ||
//------------||
gulp.task('watch', ['styles', 'scripts'], function(){
    gulp.watch('theme/scripts/*.js', ['ugly-js']);
    gulp.watch('theme/stylus/**/*.styl', ['style-watch']);
    gulp.watch('theme/views/*.pug', ['pug']);
    watch('theme/assets/**/*', function(){
        gulp.start('copy-assets');
    });
    watch('theme/fonts/**/*', function(){
        gulp.start('copy-assets');
    });
    watch('theme/scripts/vendor/**/*.js', function(){
        gulp.start('ugly-js');
    });
    watch('theme/include/**/*', function(){
        gulp.start('copy-assets');
    });
    watch('theme/*.php', function(){
        gulp.start('copy-assets');
    });
});

//---------||
//  SERVE  ||
//---------||
gulp.task('serve', ['start', 'styles', 'scripts', 'watch']);