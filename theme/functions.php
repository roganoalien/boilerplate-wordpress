<?php 
//theme support for creating menu called header
function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );
//theme support for posts thumbnails
add_theme_support( 'post-thumbnails' );
//REMOVE P TAGS AROUND IMAGES ON POSTS
function filter_ptags_on_images($content){
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');
//add exceerpts to pages
add_action('init', 'my_add_excerpts_to_pages');
function my_add_excerpts_to_pages(){
	add_post_type_support('page', 'excerpt' );
}